
public class Rectangle {
    private String name;
    private double width;
    private double height;

    public Rectangle(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(String name) {
        return name;
    }

    public double areaRectangle() {
        double ar = (width * height);
        return ar;
    }

    public double perRectangle() {
        double pr = (2 * (width + height));
        return pr;
    }

    public void printar() {
        System.out.println(name + "Area of a Rectangle is " + areaRectangle());

    }

    public void printpr() {
        System.out.println(name + "Perimeter of a Rectangle is " + perRectangle());

    }

}
