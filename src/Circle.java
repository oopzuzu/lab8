

public class Circle {
    private String name;
    private double radian;
    public Circle(String name, double radian){
        this.name = name;
        this.radian = radian;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(String name){
        return name;
    }
    public double areaCircle(){
        double ac = Math.PI * (radian*radian);
        return ac;
    }
    public double perCircle(){
        double pc = (2*Math.PI*radian);
        return pc;
    }
    public void printac(){
        System.out.println(name+"Area of a Circle is "+areaCircle());

    }
    public void printpc(){
        System.out.println(name+"Perimeter of a Circle is "+perCircle());

    }

}
