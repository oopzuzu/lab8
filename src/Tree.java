public class Tree {
    private String name;
    private int x;
    private int y;

    public Tree(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(String name) {
        return name;
    }

    public void printTree(){
        System.out.println(name+" " + "X : " + x + " " + "Y : " + y);

    }
}
